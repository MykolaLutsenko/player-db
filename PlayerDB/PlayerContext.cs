﻿using System.Data.Entity;

namespace PlayerDB
{
    public class PlayerContext :DbContext
    {
        public PlayerContext() : base("PlayerConnection")
        { }

        public DbSet<Player> Players { get; set; }
        public DbSet<Team> Teams { get; set; }
    }
}