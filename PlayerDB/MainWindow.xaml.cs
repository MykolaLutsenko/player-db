﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PlayerDB
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        PlayerContext db;

        public MainWindow()
        {
            InitializeComponent();

            db = new PlayerContext();
            db.Players.Load();
            db.Teams.Load();

            //this.DataGrid.ItemsSource = db.Players.Join(db.Teams,
            //    p => p.Id,
            //    t => t.Id,
            //    (p, t) => new
            //    {
            //        Name = p.Name,
            //        Age = p.Age,
            //        Position = p.Position,
            //        Command = p.Teams.FirstOrDefault().Name
            //    }).ToList();
            this.DataGrid.ItemsSource = db.Players.Local.ToBindingList();
        }

        private void AddBtn_Click(object sender, RoutedEventArgs e)
        {
            PlayerForm plForm = new PlayerForm();
            plForm.Title = "Добавить игрока";

            List<Team> teams = db.Teams.ToList();
            plForm.CommandsListBox.ItemsSource = teams;
            plForm.CommandsListBox.SelectedValuePath = "Id";
            plForm.CommandsListBox.DisplayMemberPath = "Name";

            plForm.ShowDialog();

            if (plForm.Name.Text.Length >0)
            {
                Player player = new Player();
                player.Age = (int) plForm.NumericUpDown.Value;
                player.Name = plForm.Name.Text;
                player.Position = plForm.Position.Text;

                teams.Clear();

                foreach (var selectedItem in plForm.CommandsListBox.SelectedItems)
                {
                    teams.Add((Team)selectedItem);
                }

                player.Teams = teams;

                db.Players.Add(player);
                db.SaveChanges();
            }
        }
        private void ChangeBtn_Click(object sender, RoutedEventArgs e)
        {
            if (DataGrid.SelectedCells.Count > 0)
            {
                int index = DataGrid.Items.IndexOf(0);
                var selectedRow = DataGrid.SelectedItems[0] as Player;
                bool converted = int.TryParse(selectedRow?.Id.ToString(),out var id);

                if(!converted)
                    return;

                Player player = db.Players.Find(id);
                PlayerForm plFormPlayer = new PlayerForm();
                plFormPlayer.Title = "Изменить игрока";
                plFormPlayer.Name.Text = player.Name;
                plFormPlayer.NumericUpDown.Value = player.Age;
                plFormPlayer.Position.Text = player.Position;

                List<Team> teams = db.Teams.ToList();
                plFormPlayer.CommandsListBox.ItemsSource = teams;
                plFormPlayer.CommandsListBox.SelectedValue = "Id";
                plFormPlayer.CommandsListBox.DisplayMemberPath = "Name";

                foreach (var team in player.Teams)
                    plFormPlayer.CommandsListBox.SelectedItem = team;

                plFormPlayer.ShowDialog();

                if (!plFormPlayer.DialogResult.Value)
                    return;

                player.Age = (int)plFormPlayer.NumericUpDown.Value;
                player.Name = plFormPlayer.Name.Text;
                player.Position = plFormPlayer.Position.Text;

                foreach (var team in teams)
                {
                    if (plFormPlayer.CommandsListBox.SelectedItems.Contains(team))
                    {
                        if (!player.Teams.Contains(team))
                        {
                            player.Teams.Add(team);
                        }
                    }
                    else
                    {
                        if (player.Teams.Contains(team))
                        {
                            player.Teams.Remove(team);
                        }
                    }
                }

                db.Entry(player).State = EntityState.Modified;
                
                db.SaveChanges();
                DataGrid.Items.Refresh();
            }
        }

        private void AddComandBtn_Click(object sender, RoutedEventArgs e)
        {
            TeamForm tmForm = new TeamForm();
            tmForm.ShowDialog();

            if(!tmForm.DialogResult.Value)
                return;

            Team team = new Team();
            team.Name = tmForm.CommandName.Text;
            team.Coach = tmForm.CouchName.Text;
            team.Players = new List<Player>();

            db.Teams.Add(team);
            db.SaveChanges();
        }

        private void DeleteBtn_Click(object sender, RoutedEventArgs e)
        {
            if (DataGrid.SelectedCells.Count > 0)
            {
                int index = DataGrid.Items.IndexOf(0);
                var selectedRow = DataGrid.SelectedItems[0] as Player;
                bool converted = int.TryParse(selectedRow.Id.ToString(),out var id);

                if(!converted)
                    return;

                Player player = db.Players.Find(id);
                db.Players.Remove(player);
                db.SaveChanges();
            }
        }
    }
}
